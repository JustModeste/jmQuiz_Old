import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ApplicationWindow {
   visible: true
   width: 400
   height: 640
   title: qsTr("jmQuiz")

   header: JmScore {
      Layout.fillWidth:  true
   }

   ColumnLayout {
       anchors.fill: parent

      JmQuestion {
          id: jmQuestion
         Layout.fillHeight: true
         Layout.fillWidth: true
      }

      JmParam {
         id: jmParam
         Layout.fillHeight: true
         Layout.fillWidth: true
         visible: false
      }

      JmConfirm {
          id: jmConfirm
          visible: false

          onValided: {
              close()
          }
          onCancel: {
              visible = false
              bBB.visible = true
          }
      }

}

   footer: JmBottomButtons {
      id: bBB
      Layout.fillWidth: true

      onParam: {
         console.log("param")
          jmQuestion.visible = false
          jmParam.visible = true
          state = 'Param'
      }

      onQuit: {
          console.log("Quit")
          jmConfirm.visible = true
          bBB.visible = false
      }
   }
}
