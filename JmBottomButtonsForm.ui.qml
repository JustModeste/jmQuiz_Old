import QtQuick 2.4
import QtQuick.Layouts 1.3

Item {
    width: 400
    height: 40
    property alias bParams: bParams
    property alias bQuit: bQuit



    RowLayout {
        id: rowLayout
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
       anchors.fill: parent


       JmButton {
          id: bParams
          radius: 5
          text: "..."
          Layout.fillHeight: true
       }

       Item {
          Layout.fillWidth: true
       }

       JmButton {
          id: bQuit
          text: qsTr("Quit")
          Layout.fillHeight: true
       }
    }
}
