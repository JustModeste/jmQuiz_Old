import QtQuick 2.4
import QtQuick.Layouts 1.3

Item {
   width: 400
   height: 40
   Rectangle {
       color: "#647eb1"
       anchors.fill: parent

      RowLayout {
         anchors.rightMargin: 10
         anchors.leftMargin: 10
         anchors.fill: parent

         Text {
            id: lScoreLabel
            text: qsTr("Score")
            Layout.fillWidth: true
         }

         Text {
            id: lScoreValue
            text: "0"
            Layout.fillWidth: true
         }

         Text {
            id: lBetLabel
            text : qsTr("Bet")
            Layout.fillWidth: true
         }

         Text {
            id: lBetValue
            text: "0"
            Layout.fillWidth: true
         }
      }
   }
}
