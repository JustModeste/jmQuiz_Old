import QtQuick 2.4

JmConfirmForm {
    signal valided
    signal cancel

    bValid.onClicked: {
        valided();
   }

    bCancel.onClicked: {
        cancel()
    }
}
