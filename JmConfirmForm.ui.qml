import QtQuick 2.4
import QtQuick.Layouts 1.3

Item {
    width: 400
    height: 100
    property alias bValid: bValid
    property alias bCancel: bCancel

    ColumnLayout {
        anchors.rightMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.topMargin: 5
        anchors.fill: parent

        Text {
            id:lText
            text: "Do you want quit ?"
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
        }

        RowLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true

            JmButton {
                id: bValid
                text: qsTr("Confirm")
                bottomColor: "#00aa00"
            }

            Item {
                Layout.fillWidth: true
            }

            JmButton {
                id: bCancel
                text: qsTr("Cancel")
                bottomColor: "#aa0000"
            }
        }
    }
}
