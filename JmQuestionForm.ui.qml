import QtQuick 2.4
import QtQuick.Layouts 1.3


Item {
    id: root
    width: 400
    height: 640
    property alias bError: bError
    property alias bCorrect: bCorrect
    property alias bAnswer1: bAnswer1
    property alias bAnswer2: bAnswer2
    property alias bAnswer3: bAnswer3
    property alias bAnswer4: bAnswer4

   ColumnLayout {
      anchors.fill: parent
      anchors.margins: 5

      JmItem {
         id: iQuestion
         Layout.fillHeight: true
         Layout.fillWidth: true
         radius: 5
      }

      RowLayout {
         JmButton {
            id: bAnswer1
            Layout.fillWidth: true
            Layout.fillHeight: true
            enabled: true

         }

         JmButton {
            id: bAnswer2
            Layout.fillHeight: true
            Layout.fillWidth: true
            enabled: true
         }
      }

      RowLayout {
         JmButton {
            id: bAnswer3
            Layout.fillHeight: true
            Layout.fillWidth: true
            enabled: true
         }

         JmButton {
            id: bAnswer4
            Layout.fillHeight: true
            Layout.fillWidth: true
            enabled: true
         }
      }
   }

   JmButton {
       id: bCorrect
       height: 100
       width: 200
       radius: 5
       text: qsTr("Correct")
       visible: false
       bottomColor: "#00ff00"
       anchors.horizontalCenter: parent.horizontalCenter
       anchors.verticalCenter: parent.verticalCenter
   }

   JmButton {
       id: bError
       height:100
       width: 200
       radius: 5
       text: qsTr("Error")
       visible: false
       bottomColor: "#ff0000"
       anchors.horizontalCenter: parent.horizontalCenter
       anchors.verticalCenter: parent.verticalCenter
   }

   states: [
       State {
           name: "Valid"

           PropertyChanges {
               target: bCorrect
               visible: true
           }

           PropertyChanges {
               target: bAnswer1
               enabled: false
           }

           PropertyChanges {
               target: bAnswer2
               enabled: false
           }

           PropertyChanges {
               target: bAnswer3
               enabled: false
           }

           PropertyChanges {
               target: bAnswer4
               enabled: false
           }
       },
       State {
           name: "Error"

           PropertyChanges {
               target: bError
               visible: true
           }

           PropertyChanges {
               target: bAnswer1
               enabled: false
           }

           PropertyChanges {
               target: bAnswer2
               enabled: false
           }

           PropertyChanges {
               target: bAnswer3
               enabled: false
           }

           PropertyChanges {
               target: bAnswer4
               enabled: false
           }
       }

   ]
}
