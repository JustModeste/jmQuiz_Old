#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QDebug>

#include "jmDatabase/Classes/jmdatabase.h"

int main(int argc, char *argv[])
{
    jmDatabase *db = new jmDatabase("localhost", "jmquiz.db", "jmquiz", "jmquiz");

    if (db->Open()) {
        qDebug() << "Database 'jmquiz.db' is open";
    } else {
        return -2;
    }
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
