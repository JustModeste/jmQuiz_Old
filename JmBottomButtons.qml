import QtQuick 2.4

JmBottomButtonsForm {
    signal quit
    signal  param

    bParams.onClicked: {
       param()
    }

    bQuit.onClicked: {
       quit()
    }
}
